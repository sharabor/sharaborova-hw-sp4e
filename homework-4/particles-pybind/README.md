# sharaborova-hw-sp4e repository

This git directory was created in order to work on the last exercise (Homework 4) for course **"Scientific Programming for Eginners (SP4E)"**. It also allows access to the teacher and his team to evaluate the work requested.

## Description
As part of the PhD training, I must take a course entitled Scientific Programming for Eginners (SP4E). In this repository is presented only the last exercise, to see the results of previous work please follow the link: https://gitlab.epfl.ch/tomat/sharaborova-tomat .


## Homework N°4

The goal of this exercise is to use the external library Pybind11 to create Python bindings of the C++ Particles’ code. More information here: https://gitlab.epfl.ch/sharabor/sharaborova-hw-sp4e/-/blob/main/homework-4/particles-pybind/sujet.pdf 

### How to build

cd particles-pybind

mkdir build

cd build

ccmake ..

make



## Exercises

### Exercise 1

The answer is presented as a comment in the code file pypart.cc

### Exercise 2

addCompute function gets an argument of a type Compute in a shared pointer, that is why the template type should be pointed out and a special holder type should be denoted for managing the reference to the obgect accordingly. Thats is for objects from Compute were wrapped into theor designated types as std::shared_ptr<...>. 

### Exercise 3

The solution is presented in the file pypart.cc

### Exercise 4

### Exercise 5

The solution is presented in the file main.py

### Exercise 6

The solution is presented in the file main.py


## Authors

Ms. Elizaveta Sharaborova (elizaveta.sharaborova@epfl.ch) PhD student

## Project status

- **Homework number 1**: Realized between October 12 and 19, 2022
- **Homework number 2**: Realized between October 27 and November 02, 2022
- **Homework number 3**: Realized between December 1 and December 15, 2022
- **Homework number 4**: Realized between January 4 and January 18, 2023
