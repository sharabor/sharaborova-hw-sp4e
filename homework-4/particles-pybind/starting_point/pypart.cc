#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "system_evolution.hh"
#include "compute.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "compute_temperature.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  //ParticlesFactory
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
          .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
          //Attempting to bind ParticlesFactoryInterface::createSimulation will cause an error since the compiler does not know which method 
          //the user intended to select. We can disambiguate by casting them to function pointers. Binding multiple functions to the same 
          //Python name automatically creates a chain of function overloads that will be tried in sequence.
          //Overloading of createComputes used to set the atributes before the createSimultation method will be called.
          //Attribute of createComputes ised by daughter classes to assosiate forces to a simuation.
          //In ase of no attributes for createComputes the default computing is called and it makes the assosiation of forces in particles.
          .def("createSimulation", [](ParticlesFactoryInterface& self, const std::string& fname, Real timestep) -> SystemEvolution& {
            return self.createSimulation(fname, timestep);},
            py::return_value_policy::reference)
          .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
          .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution);
  
  //MaterialPointsFactory
  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
          .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);

  //PlanetsFactory
  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
          .def("getInstance",  &PlanetsFactory::getInstance, py::return_value_policy::reference);
  
  //PingPongBallsFactory
  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
          .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference)
          .def("createParticle", &PingPongBallsFactory::createParticle, py::return_value_policy::reference);

  //Compute
  py::class_<Compute,std::shared_ptr<Compute>>(m, "Compute");

  //ComputeInteraction
  py::class_<ComputeInteraction,Compute,std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction");

  //ComputeGravity
  py::class_<ComputeGravity,ComputeInteraction,std::shared_ptr<ComputeGravity>>(m,"ComputeGravity")
          .def(py::init<>())
          .def("setG", &ComputeGravity::setG);
  
  //ComputeTemperature
  py::class_<ComputeTemperature,Compute,std::shared_ptr<ComputeTemperature>>(m,"ComputeTemperature")
          .def(py::init<>())
          .def_property("conductivity", &ComputeTemperature::getConductivity,[](ComputeTemperature& c, Real val) {c.getConductivity()=val;})
          .def_property("L", &ComputeTemperature::getL,[](ComputeTemperature& c, Real val) {c.getL()=val;})
          .def_property("capacity", &ComputeTemperature::getCapacity,[](ComputeTemperature& c, Real val) {c.getCapacity()=val;})
          .def_property("density", &ComputeTemperature::getDensity,[](ComputeTemperature& c, Real val) {c.getDensity()=val;})
          .def_property("deltat", &ComputeTemperature::getDeltat,[](ComputeTemperature& c, Real val) {c.getDeltat()=val;});

  //ComputeVerletIntegration
  py::class_<ComputeVerletIntegration,Compute,std::shared_ptr<ComputeVerletIntegration>>(m,"ComputeVerletIntegration")
          .def(py::init<Real>())
          .def("addIteraction", &ComputeVerletIntegration::addInteraction);

  //CsvWriter
  py::class_<CsvWriter,Compute,std::shared_ptr<CsvWriter>>(
    m,"CsvWriter")
    .def(py::init<const std::string &>())
    .def("write", &CsvWriter::write);

  //System
  py::class_<System>(m,"System")
    .def(py::init<>());

  //System evolution
  py::class_<SystemEvolution>(m, "SystemEvolution")
          .def("getSystem",&SystemEvolution::getSystem)
          .def("setNSteps",&SystemEvolution::setNSteps)
          .def("setDumpFreq",&SystemEvolution::setDumpFreq)
          .def("evolve",&SystemEvolution::evolve)
          .def("addCompute",&SystemEvolution::addCompute);


  // bind the routines here
}
